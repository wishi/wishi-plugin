# WISHI plugin #

### Setup ###

Add a script tag with wishi plugin url
```
<script src="wishi-plugin-url"  wishi-key="% your wishi key %"></script>
```

Once the script loaded, the plugin will be available in `window.WishiChat`.

Whenever using WISHI plugin you should:
1. Make sure that the div containing the plugin is available in the dome.

2. Create a `new WishiChat(element, clientId, clientInfo, clientCartItems, orderHistoryItems)` instant.

3. Listen to `cart-item-added`, `cart-item-removed`, `new-look` events.

4. Stop client by calling `.stop()`.


### Mount the chat ###

Get the element that the chat should be mounted to.
```
var chatDiv = document.document.getElementById('elementId');
```

Create a new WishiChat instance

###### Client Id ######
Partner identifier for the client, for client first use, a new styling
session will be generated. For returning clients the clientId will be used to identify the clients and continue their session.

###### Client Info ######
Client required and optional info
```
{
    firstName: 'Morty', // Required
    lastName: 'Smith', // Required
	email: 'Morty.Smith@gmail.com', // Required
	birthday: '01-01-2004', // Optional
    phone: '+1-212-999999', // Optional
	gender: 'female', // Optional
	country: 'US', // Optional
	currency: 'USD' // Optional
}
```

###### Cart items ######
array of client cart items ids
```
['itemId-1', 'itemId-2', 'itemId-3']
```

###### Order History items ######
array of client order history items ids
```
['itemId-1', 'itemId-2', 'itemId-3']
```

###### Initialize chat instance ######
The chat instance lifetime will be from constructing till calling it stop function.
```
var wishiChat = var wishiChat = new WishiChat(chatDiv, clientId, clientInfo, cartItems, orderHistoryItems);
```
This will open a connection with our chat and mount the chat component into the provided element.

###### stop ######
Calling stop() closes the connection to wishi and unmount the plugin from the element provided on initialization
```
wishiChat.stop();
```

### Listen to events ###
Partner website will be notified upon different user interactions by registering
to chat events and passing a handler that will be called when ever the event triggered.

###### cart-item-added ######
when the user clicks on add button, a cart-item-added event will be triggered
```
wishiChat.on('cart-item-added', function (itemId) {
	// handle Cart item of itemId that was added to client cart
});
```

###### cart-item-removed ######
when the user clicks on remove button, a cart-item-removed event will be triggered
```
wishiChat.on('cart-item-removed', function (itemId) {
	// handle Cart item of itemId that was removed from client cart
});
```

###### new-look ######
when the user receives a new look, a new-look event will be triggered
```
wishiChat.on('new-look', function () {
	// client just received a new look
});
```
